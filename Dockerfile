FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code
WORKDIR /app/code

# renovate: datasource=github-releases depName=traccar/traccar versioning=regex:^(?<major>\d+)\.(?<minor>\d+)\.?(?<patch>\d+)?$ extractVersion=^v(?<version>.+)$
ARG TRACCAR_VERSION=6.6

RUN wget https://github.com/traccar/traccar/releases/download/v${TRACCAR_VERSION}/traccar-linux-64-${TRACCAR_VERSION}.zip -O traccar.zip && \
    unzip traccar.zip && \
    ./traccar.run --target /app/code/ --noexec && \
    rm README.txt traccar.zip traccar.run

RUN rm /app/code/conf/traccar.xml && ln -s /app/data/traccar.xml /app/code/conf/traccar.xml && \
    rm -rf /app/code/logs && ln -s /run/traccar/logs /app/code/logs

COPY start.sh traccar.xml.template /app/pkg/

CMD [ "/app/pkg/start.sh" ]
