#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.EMAIL || !process.env.PASSWORD) {
    console.log('USERNAME, EMAIL and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = 20000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const DEVICE_NAME = 'FancyDevice';
    const DEVICE_IDENTIFIER = 'device1';
    const ADMIN_USERNAME = 'admin@cloudron.local';
    const ADMIN_PASSWORD = 'admin';
    const USERNAME = process.env.USERNAME;
    const EMAIL = process.env.EMAIL;
    const PASSWORD = process.env.PASSWORD;

    let browser, app;
    let athenticated_by_oidc = false;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function login(emailOrUsername, password) {
        await browser.get(`https://${app.fqdn}/login`);
        await waitForElement(By.xpath('//input[@name="email"]'));
        await browser.findElement(By.xpath('//input[@name="email"]')).sendKeys(Key.CONTROL + 'a' + Key.BACK_SPACE + Key.COMMAND + 'a' + Key.BACK_SPACE);
        await browser.findElement(By.xpath('//input[@name="email"]')).sendKeys(emailOrUsername);
        await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);
        await browser.findElement(By.xpath('//button[text()="Login"]')).click();
        await browser.sleep(3000);
        await waitForElement(By.xpath('//span[text()="Account"]'));
    }

    async function loginOIDC(username, password, alreadyAuthenticated = true) {
        browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}/login`);
        await browser.sleep(2000);

        await waitForElement(By.xpath('//button[contains(., "Login with OpenID")]'));
        await browser.findElement(By.xpath('//button[contains(., "Login with OpenID")]')).click();
        await browser.sleep(2000);

        if (!alreadyAuthenticated) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();
        }

        await browser.sleep(3000);
        await waitForElement(By.xpath('//span[text()="Account"]'));
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath('//span[text()="Account"]'));
        await browser.findElement(By.xpath('//span[text()="Account"]')).click();
        await browser.sleep(1000);
        await waitForElement(By.xpath('//p[text()="Logout"]'));
        await browser.findElement(By.xpath('//p[text()="Logout"]')).click();
        await waitForElement(By.xpath('//input[@name="email"]'));
    }

    async function addDevice() {
        await browser.get(`https://${app.fqdn}/settings/device`);
        await waitForElement(By.id(':r3:'));
        await browser.findElement(By.id(':r3:')).sendKeys(DEVICE_NAME);
        await browser.findElement(By.id(':r4:')).sendKeys(DEVICE_IDENTIFIER);
        await browser.findElement(By.xpath('//button[text()="Save"]')).click();
        await waitForElement(By.xpath(`//span[text()="${DEVICE_NAME}"]`));
    }

    async function deviceExists() {
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath(`//span[text()="${DEVICE_NAME}"]`));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    // no sso
    it('install app (no sso)', function () { execSync(`cloudron install --no-sso --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login as admin', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('can add device', addDevice);
    it('device exists', deviceExists);
    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // sso
    it('install app (sso)', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login as admin', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('can add device', addDevice);
    it('device exists', deviceExists);
    it('can logout', logout);

    it('can login as normal user via OIDC', loginOIDC.bind(null, process.env.USERNAME, process.env.PASSWORD, false));
    it('can logout', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`); });

    it('can login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('device exists', deviceExists);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('device exists', deviceExists);
    it('can logout', logout);

    it('can login as normal user via OIDC', loginOIDC.bind(null, process.env.USERNAME, process.env.PASSWORD));
    it('can logout', logout);

    it('move to different location', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);
    it('can login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('device exists', deviceExists);
    it('can logout', logout);

    it('can login as normal user via OIDC', loginOIDC.bind(null, process.env.USERNAME, process.env.PASSWORD));
    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app for update', function () { execSync(`cloudron install --appstore-id org.traccar.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('can add device', addDevice);
    it('device exists', deviceExists);
    it('can logout', logout);

    it('can login as normal user via OIDC', loginOIDC.bind(null, process.env.USERNAME, process.env.PASSWORD));
    it('can logout', logout);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });

    it('can login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('device exists', deviceExists);
    it('can logout', logout);

    // OIDC login
    it('can login as normal user via OIDC', loginOIDC.bind(null, process.env.USERNAME, process.env.PASSWORD));
    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
