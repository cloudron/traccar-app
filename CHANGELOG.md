[0.1.0]
* Initial version

[0.2.0]
* Update manifest

[0.3.0]
* Log to stdout

[0.4.0]
* Switch from postgres to mysql. This will break previous installations, please start afresh
* Optional SSO

[0.4.1]
* Update traccar to 5.1
* [Full changelog](https://github.com/traccar/traccar/releases/tag/v5.1)

[0.5.0]
* Update traccar to 5.2
* [Full changelog](https://github.com/traccar/traccar/releases/tag/v5.2)

[0.6.0]
* Update traccar to 5.3
* [Full changelog](https://github.com/traccar/traccar/releases/tag/v5.3)

[1.0.0]
* First stable release
* Add support for gl200 protocol port 5004

[1.0.1]
* Add GPS103 port 5001

[1.0.2]
* Add TAIP port 5031

[1.0.3]
* Update traccar to 5.4
* [Full changelog](https://github.com/traccar/traccar/releases/tag/v5.4)

[1.1.0]
* Update traccar to 5.5
* [Full changelog](https://github.com/traccar/traccar/releases/tag/v5.5)

[1.2.0]
* Update base image to 4.0.0

[1.2.1]
* Add H02 port 5013

[1.2.2]
* Update traccar to 5.6
* [Full changelog](https://github.com/traccar/traccar/releases/tag/v5.6)

[1.3.0]
* Update traccar to 5.7
* [Full changelog](https://github.com/traccar/traccar/releases/tag/v5.7)

[1.4.0]
* Add support for Telefonika TMT250 Port 5027

[1.5.0]
* Update traccar to 5.8
* [Full changelog](https://github.com/traccar/traccar/releases/tag/v5.8)

[1.5.1]
* Update traccar to 5.9
* [Full changelog](https://github.com/traccar/traccar/releases/tag/v5.9)

[1.6.0]
* Update base image to 4.2.0

[1.7.0]
* Update traccar to 5.10
* [Full changelog](https://github.com/traccar/traccar/releases/tag/v5.10)
* [Release Announcement](https://www.traccar.org/blog/traccar-5-10/)
* Two-factor authentication using TOTP
* Event to notify about sent queued commands
* Option to access last position in computed attributes
* Unification of OBD speed units
* Event buffer limit in the web app (should improve performance)
* Automatic update check for the web app

[1.7.1]
* Update traccar to 5.11
* [Full changelog](https://github.com/traccar/traccar/releases/tag/v5.11)
* Update web submodule

[1.7.2]
* Update traccar to 5.12
* [Full changelog](https://www.traccar.org/blog/traccar-5-12/)

[1.7.3]
* Only create admin account on fresh installation

[1.8.0]
* Update traccar to 6.0
* [Full changelog](https://www.traccar.org/blog/traccar-6-0/)

[1.9.0]
* Update traccar to 6.1
* [Full changelog](https://www.traccar.org/blog/traccar-6-1/)

[1.10.0]
* Update traccar to 6.2
* [Full changelog](https://www.traccar.org/blog/traccar-6-2/)
* Computed attributes can now be ordered using priority
* Support token authentication for WebSocket
* Allow regular users to edit hours and total distance
* Support for right-to-left languages on map
* Terms and privacy policy option
* Engine hours are now calculated after computed attributes
* Default configuration file is now removed to avoid confusion and misconfiguration

[1.11.0]
* Migrate to OIDC login

[1.11.1]
* Fix bug where OIDC configuration was cleared in non-sso mode

[1.12.0]
* Update traccar to 6.3
* [Full changelog](https://www.traccar.org/blog/traccar-6-3/)
* Health check on the volume of stored messages
* Fix push notifications for read-only users
* Use official Google Map tiles with an API key
* Add Google traffic overlay
* New reverse geocoding options
* Faster device removal without removing history
* Avoid duplicated tasks with horizontal scaling
* Add a users column to the device list
* Clear attribute on null computation result

[1.13.0]
* Update traccar to 6.4
* [Full changelog](https://www.traccar.org/blog/traccar-6-4/)
* critical fix for device freezing issue

[1.14.0]
* Update traccar to 6.5
* [Full changelog](https://www.traccar.org/blog/traccar-6-5/)
* Handling processing errors to fix freezing devices
* Option to use linked driver as the current driver
* Support multiple alarms in a single position
* Improve web app loading experience
* Google Maps fallback option with no API key
* Wialon position forwarding option
* Fix map attribution control

[1.15.0]
* Update traccar to 6.6
* [Full Changelog](https://www.traccar.org/blog/traccar-6-6/)
* Managers can now send announcements
* Fixes for multiline SMS notifications
* Early and late computed attributes
* Improvements to user session security
* Support for custom navigation links
* Enhanced speed color scheme
* Collapsible settings and reports drawer
* Improved device card UI

