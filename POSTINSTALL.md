This app is pre-setup with an admin account. The initial credentials are:

**Username**: admin@cloudron.local<br/>
**Password**: admin<br/>

<sso>
By default, Cloudron users have regular users permissions. Permissions can be updated on the user profile page in the admin back-end.
</sso>
