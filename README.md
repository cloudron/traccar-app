# Traccar Cloudron App

This repository contains the Cloudron app package source for [Traccar](https://traccar.org/).

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=org.traccar.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id org.traccar.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd traccar-app

cloudron build
cloudron install
```

## Testing

The e2e tests are located in the `test/` folder and require [nodejs](http://nodejs.org/). They are creating a fresh build, install the app on your Cloudron, perform tests, backup, restore and test if the repos are still ok. The tests expect port 29418 to be available.

```
cd traccar-app/test

npm install
USERNAME=<cloudron username> PASSWORD=<cloudron password> mocha --bail test.js
```


## Devices

https://www.traccar.org/devices/

Traccar client | osmand | port 5055 (http)
Tracker for Traccar | t55 | port 5005 | sends odb2 data
Owntracks | owntracks | port 5144 | https://owntracks.org/booklet/features/traccar/

