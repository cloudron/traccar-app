#!/bin/bash

set -eu -o pipefail

mkdir -p /run/traccar/logs /app/data/media

echo -e "[client]\npassword=${CLOUDRON_MYSQL_PASSWORD}" > /run/traccar/mysql-extra
readonly mysql="mysql --defaults-file=/run/traccar/mysql-extra --user=${CLOUDRON_MYSQL_USERNAME} --host=${CLOUDRON_MYSQL_HOST} -P ${CLOUDRON_MYSQL_PORT} ${CLOUDRON_MYSQL_DATABASE}"

wait_for_table() {
    ret=`$mysql --skip-column-names -s -e "SHOW TABLES LIKE '$1';"`
    while [ "$ret" != "$1" ]; do
        echo "=> Table $1 not yet created, waiting ..."
        sleep 1;
        ret=`$mysql --skip-column-names -s -e "SHOW TABLES LIKE '$1';"`
        echo "ret was ${ret}"
    done
}

disable_registration() {
    wait_for_table tc_servers;

    echo "==> Disabling registration"
    $mysql -e "UPDATE tc_servers SET registration=0 WHERE id=1"
}

ensure_admin_account() {
    wait_for_table tc_users;

    echo "==> Ensure admin account"
    count=`$mysql --skip-column-names -s -e "SELECT COUNT(*) FROM tc_users;"`
    if [[ "$count" = "0" ]]; then
        echo "==> Create initial admin account"
        # Values are from https://github.com/traccar/traccar/blob/master/schema/changelog-3.3.xml#L179 which is not used anymore, but we still want the admin account
        $mysql -e "INSERT INTO tc_users (name, email, hashedpassword, salt, administrator) VALUES ('admin', 'admin@cloudron.local', 'D33DCA55ABD4CC5BC76F2BC0B4E603FE2C6F61F4C1EF2D47', '000000000000000000000000000000000000000000000000', TRUE)"
    fi
}

echo "=> Ensure traccar.xml config"
if [[ ! -f /app/data/traccar.xml ]]; then
    cp /app/pkg/traccar.xml.template /app/data/traccar.xml
    [[ -z "${CLOUDRON_OIDC_ISSUER:-}" ]] && sed -e 's/^.*openid\..*$//g' -i /app/data/traccar.xml # do this only first run
fi

echo "=> Ensure database settings"
# database (https://www.traccar.org/mysql/)
xmlstarlet ed --inplace \
    --update '//properties/entry[@key="database.url"]' -v "jdbc:mysql://${CLOUDRON_MYSQL_HOST}:${CLOUDRON_MYSQL_PORT}/${CLOUDRON_MYSQL_DATABASE}?serverTimezone=UTC&allowPublicKeyRetrieval=true&useSSL=false&allowMultiQueries=true&autoReconnect=true&useUnicode=yes&characterEncoding=UTF-8&sessionVariables=sql_mode=''" \
    --update '//properties/entry[@key="database.user"]' -v "${CLOUDRON_MYSQL_USERNAME}" \
    --update '//properties/entry[@key="database.password"]' -v "${CLOUDRON_MYSQL_PASSWORD}" \
    /app/data/traccar.xml

# origin
xmlstarlet ed --inplace --update '//properties/entry[@key="web.url"]' -v "${CLOUDRON_APP_ORIGIN}" /app/data/traccar.xml

# OIDC
if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
    echo "=> Ensure OIDC settings"
    # CLOUDRON_OIDC_PROVIDER_NAME is not supported
    xmlstarlet ed --inplace \
        --update '//properties/entry[@key="openid.clientId"]' -v "${CLOUDRON_OIDC_CLIENT_ID}" \
        --update '//properties/entry[@key="openid.clientSecret"]' -v "${CLOUDRON_OIDC_CLIENT_SECRET}" \
        --update '//properties/entry[@key="openid.issuerUrl"]' -v "${CLOUDRON_OIDC_ISSUER}" \
        --update '//properties/entry[@key="openid.authUrl"]' -v "${CLOUDRON_OIDC_AUTH_ENDPOINT}" \
        --update '//properties/entry[@key="openid.tokenUrl"]' -v "${CLOUDRON_OIDC_TOKEN_ENDPOINT}" \
        --update '//properties/entry[@key="openid.userInfoUrl"]' -v "${CLOUDRON_OIDC_PROFILE_ENDPOINT}" \
        /app/data/traccar.xml
fi

# email
echo "=> Ensure mail settings"
xmlstarlet ed --inplace \
    --update '//properties/entry[@key="mail.smtp.host"]' -v "${CLOUDRON_MAIL_SMTP_SERVER}" \
    --update '//properties/entry[@key="mail.smtp.port"]' -v "${CLOUDRON_MAIL_SMTP_PORT}" \
    --update '//properties/entry[@key="mail.smtp.starttls.enable"]' -v "false" \
    --update '//properties/entry[@key="mail.smtp.from"]' -v "${CLOUDRON_MAIL_FROM}" \
    --update '//properties/entry[@key="mail.smtp.auth"]' -v "true" \
    --update '//properties/entry[@key="mail.smtp.username"]' -v "${CLOUDRON_MAIL_SMTP_USERNAME}" \
    --update '//properties/entry[@key="mail.smtp.password"]' -v "${CLOUDRON_MAIL_SMTP_PASSWORD}" \
    /app/data/traccar.xml

(disable_registration; ensure_admin_account) &

chown -R cloudron /run/traccar /app/data

# https://www.traccar.org/optimization/
echo "=> Start traccar-server"
exec gosu cloudron:cloudron /app/code/jre/bin/java -Xmx1G -jar tracker-server.jar /app/code/conf/traccar.xml
